/*
*   GLPKHELPER
*   THIS CLASS HAVE THE POURPOSE TO CREATE A SIMPLE WAY
*   TO CALCULATE SIMPLEX USING GLPK FOR JAVA
*   
*   CREATE BY:
*   DANIEL GUNNA 
*   LEONARDO A. F. PALIS
*   MARCELO RODRIGUES DOS SANTOS JUNIOR
*/

import org.gnu.glpk.GLPK;
import org.gnu.glpk.GLPKConstants;
import org.gnu.glpk.GlpkException;
import org.gnu.glpk.SWIGTYPE_p_double;
import org.gnu.glpk.SWIGTYPE_p_int;
import org.gnu.glpk.glp_prob;
import org.gnu.glpk.glp_smcp;
import java.util.List;
import java.util.ArrayList;


public class GlpkHelper {

    private static void parseExpressionsToGlpkFormat(ObjectiveFunction of, Restriction[] restrictions) {
        
        glp_prob lp;
        glp_smcp parm;
        SWIGTYPE_p_int ind;
        SWIGTYPE_p_double val;
        int ret;
        int cols = 0;
        int rows = 0;

        //VERIFY HOW MANY COLUMS AND ROWS EXITS.
        //COLS -> RESTRICTIONS THAT ARE OF FORMAT: EX: 2X1 <= 300
        for (int i = 0; i < restrictions.length; i++) {
            int count = 0;
            for (int j = 0; j < restrictions[i].getCoefficients().length; j++) {
                if(restrictions[i].getCoefficients()[j] != 0){
                    count++;
                }
            } 
            if (count == 1) {
                cols++;
            } else {
                rows++;
            }
            
        }

        //CREATE COLS AND ROWS
        int[] selectedCols = new int[cols];
        int[] selectedRows = new int[rows];

        int auxj = 0;
        int indexi = 0;
        int indexj = 0;

        //LETS COUNT AND ADD THE POSITION THAT WAS FOUND THE COLS AND ROWS
        for (int i = 0; i < restrictions.length; i++) {
            int count = 0;
            auxj = 0;
            for (int j = 0; j < restrictions[i].getCoefficients().length; j++) {
                if(restrictions[i].getCoefficients()[j] != 0){
                    auxj = j;
                    count++;
                }
            } 
            if (count == 1) {
                selectedCols[indexi++] = i;
            } else {
                selectedRows[indexj++] = i;
            }
            
        }

        try {

            //START THE PROBLEM!
            lp = GLPK.glp_create_prob();
            System.out.println("Problem created");
            GLPK.glp_set_prob_name(lp, "myProblem");

            //VARIABLE JUST TO CONTROL WHEN CALCULATE PARAS TO ROWS OR COLS
            boolean isCol = false;
            boolean isRow = false;
            int countRow = 1;

            //CREATE ROWS AND COLS
            GLPK.glp_add_rows(lp, rows);
            GLPK.glp_add_cols(lp, cols);
             
             //VERIFY IF THE ELEMENT IS A ROW OR A COLS
             for (int i = 0; i < restrictions.length; i++) {

                for (int j = 0;j < selectedCols.length; j++) {
                    if (i == selectedCols[j]) {
                        isCol = true;
                        isRow = false;
                        break;
                    }
                }

                for (int j = 0;j < selectedRows.length; j++) {
                    if (i == selectedRows[j]) {
                        isRow = true;
                        isCol = false;
                        break;
                    }
                }


                if(isCol){
                     //VISIT ALL COEFFICIENTS OS RESTRICTION'S ARRAY. 
                     //AFTER, TO COLS VERIFY THE TYPE OF RESTRICTION (>, <, >=, <=, =)
                     for(int j = 0; j < restrictions[i].getCoefficients().length; j++){
                        //HAVE THE FORMAT: X1 >= 300
                        if(restrictions[i].getCoefficients()[j] != 0){
                            GLPK.glp_set_col_name(lp, j+1, "x"+(j+1));
                            GLPK.glp_set_col_kind(lp, j+1, GLPKConstants.GLP_CV);
                            double value = restrictions[i].getFreeElement();
                            if (restrictions[i].getType() == Restriction.Type.EQUAL) {
                                GLPK.glp_set_col_bnds(lp, j+1, GLPKConstants.GLP_FX, value, 0);
                            } else if (restrictions[i].getType() == Restriction.Type.LESS_THAN_EQUAL) {
                                GLPK.glp_set_col_bnds(lp, j+1, GLPKConstants.GLP_DB, 0, value);
                            } else if (restrictions[i].getType() == Restriction.Type.GREATER_THAN_EQUAL) {
                                System.out.println("TESTANDO1");
                                GLPK.glp_set_col_bnds(lp, j+1, GLPKConstants.GLP_DB, value, Integer.MAX_VALUE);
                            }

                            break;
                        }

                     }  
                 
                }else{
                    //HAVE THE FORMAT: X1 + X2 <= 900
                    double value = restrictions[i].getFreeElement();
                   
                    ind = GLPK.new_intArray(restrictions[i].getCoefficients().length);
                    val = GLPK.new_doubleArray(restrictions[i].getCoefficients().length);
                    
                    //ET ROWS
                    GLPK.glp_set_row_name(lp, i+1, "c"+(i+1));

                    if (restrictions[i].getType() == Restriction.Type.EQUAL) {
                        GLPK.glp_set_row_bnds(lp, i+1, GLPKConstants.GLP_FX, value, 0);
                    } else if (restrictions[i].getType() == Restriction.Type.LESS_THAN_EQUAL) {
                        GLPK.glp_set_row_bnds(lp, i+1, GLPKConstants.GLP_DB, 0, value);
                    } else if (restrictions[i].getType() == Restriction.Type.GREATER_THAN_EQUAL) {
                        System.out.println("TESTANDO2");
                        GLPK.glp_set_row_bnds(lp, i+1, GLPKConstants.GLP_DB, value, Integer.MAX_VALUE);
                    }
                        
                    for (int j = 0; j < restrictions[i].getCoefficients().length; j++) {
                          GLPK.intArray_setitem(ind, j+1, j+1);
                          GLPK.doubleArray_setitem(val, j+1, restrictions[i].getCoefficients()[j]);
                    }

                    GLPK.glp_set_mat_row(lp, countRow++, restrictions[i].getCoefficients().length, ind, val);
                    
                    // Free memory
                    GLPK.delete_intArray(ind);
                    GLPK.delete_doubleArray(val);
                }                    
            }

            //SO, LETS DEFINE OBJECTIVE FUNCTION
            GLPK.glp_set_obj_name(lp, "z");
            System.out.println("TYPE: "+(of.getType() == ObjectiveFunction.Type.MAXIMIZATION));
            //VERIFY IF OBJECTVE FUBCTIO OPERATOR WANTS TO CALCULATE MAXIMIZATION OR MINIMIZATION
            if(of.getType() == ObjectiveFunction.Type.MAXIMIZATION){
                GLPK.glp_set_obj_dir(lp, GLPKConstants.GLP_MAX);
            }else{
                GLPK.glp_set_obj_dir(lp, GLPKConstants.GLP_MIN);
            }   

            //SET THE FREE ELEMENT OF OBJECTIVE FUNCTION
            GLPK.glp_set_obj_coef(lp, 0, of.getFreeElement());
            for(int i = 0; i < of.getCoefficients().length; i++){
                GLPK.glp_set_obj_coef(lp, i+1, of.getCoefficients()[i]);
            }

            //START TO SOLVE
            parm = new glp_smcp();
            GLPK.glp_init_smcp(parm);
            ret = GLPK.glp_simplex(lp, parm);

            //PRINT SOLUTION
            if (ret == 0) {
                write_lp_solution(lp);
            } else {
                System.out.println("The problem could not be solved");
            }

            //FREE MEMORY
            GLPK.glp_delete_prob(lp);
             
                    
        } catch (GlpkException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * write simplex solution
     * @param lp problem
     */
    static void write_lp_solution(glp_prob lp) {
        int i;
        int n;
        String name;
        double val;

        name = GLPK.glp_get_obj_name(lp);
        val = GLPK.glp_get_obj_val(lp);
        System.out.print(name);
        System.out.print(" = ");
        System.out.println(val);
        n = GLPK.glp_get_num_cols(lp);
        for (i = 1; i <= n; i++) {
            name = GLPK.glp_get_col_name(lp, i);
            val = GLPK.glp_get_col_prim(lp, i);
            System.out.print(name);
            System.out.print(" = ");
            System.out.println(val);
        }
    }


    public static void main(String[] args) throws Exception {
       
        //HERE, YOU CREATE THE OBJECTIVE FUNCTION. 
        //FORMAT: MAX/MIN expression
        ObjectiveFunction of = new ObjectiveFunction("MAX  5x1 + 3.5x2");

        //NOW, YOU NEED CREATE THE RESTRICTIONS
        List<Restriction> r = new ArrayList<>();
        r.add(new Restriction("1.5x1 + x2 <= 400", 2));
        r.add(new Restriction("1.5x1 + x2 <= 400", 2));
        r.add(new Restriction("x1 <= 150", 2));
        r.add(new Restriction("x2 <= 300", 2));
        List<Integer> lista = new ArrayList<>();

        //TO SOLVE, WE NEED CREATE RESTRINCTIONS FOR VARIABLES THAN NOT ARE OF FORMAT: X >/</>=/<=/= FREE ELEMENT
        //**IMPORTANT** IF THIS STEP DONT BE IMPROVED, THE GLPK DONT WORKS
        //SO, LETS VERIFY...
        for (int i = 0; i < r.size(); i++) {
            if (r.get(i).getVariableNames().size() == 1) {
                lista.add(Integer.valueOf(r.get(i).getVariableNames().get(0).replace("x","")));
            }
        }
        for (int i = 1; i <= of.getCoefficients().length; i++) {
            if (!lista.contains(i)) {
               r.add(new Restriction("x"+i+" <= "+Integer.MAX_VALUE, 2)); 
            }
        }

        //HERE WE CALL THE FUNCTION TO SOLVE IT!
        parseExpressionsToGlpkFormat(of,r.toArray(new Restriction[0]));
    }
}
