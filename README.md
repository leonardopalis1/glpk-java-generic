TUTORIAIS PARA INSTALAR O GLPK:

http://glpk-java.sourceforge.net/

http://glpk-java.sourceforge.net/gettingStarted.html **RECOMENDO

https://gist.github.com/huberflores/2d422581dc6657badc21

DOCUMENTAÇÃO DO GLPK:

http://kam.mff.cuni.cz/~elias/glpk.pdf

INSTRUÇÕES PARA RODAR O GLPKHELPER:

DENTRO DO DIRETÓRIO "lp", ABRIR A CLASSE GlpkHelper.java. NA FUNÇÃO MAIN, COLOCAR A FUNCÇÃO OBJETIVO DESEJADA COM AS RESTRIÇÕES ABAIXO, CONFORME 
FOI FEITO PARA O EXEMPLO:
****************************
*    MAX Z = 5x1 + 3.5x2   *
*    1.5x1 + x2 <= 400     *
*    x1 <= 150             *
*    x2 <= 300	 	   *
****************************
PARA COMPILAR O PROGRAMA, RODAR NO TERMINAL (LINUX) O SEGUINTE COMANDO:
javac -classpath /usr/local/share/java/glpk-java.jar *.java

PARA RODAR O PROGRAMA, RODAR NO TERMINAL O SEGUINTE COMANDO:
java -Djava.library.path=/usr/local/lib/jni/  -classpath /usr/local/share/java/glpk-java.jar:  GlpkHelper 

************************************
ENCAMINHE SUAS DÚVIDAS PARA O EMAIL:
     leonardoafpalis@gmail.com
************************************